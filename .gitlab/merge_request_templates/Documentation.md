**1. What this MR add or fix in the documentation (goal of this change)?** 




**2. Make sure that you've checked the boxes below before you submit MR:**
- [ ] I have read [Contribution guidelines](CONTRIBUTING.md)
- [ ] I have checked for typos and/or erroneous URLs

**3. Remember that this is licensed under AGPL, by submitting your change you agree with that**


**You don't need to change any options below, just submit and your merge request will be reviewed by our team**
