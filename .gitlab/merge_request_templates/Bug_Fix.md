**1. What is the goal of this change?**



**2. Explain how your solution works**




**3. Make sure that you've checked the boxes below before you submit MR:**
[] I have read Contribution guidelines
[] I am providing the tests (or the available tests are still enough)
[] I followed the [Rails style guide](https://github.com/rubocop-hq/rails-style-guide)

**4. Are there potential risks or drawbacks from this changes?**



**5. Remember that this is licensed under AGPL, by submitting your change you agree with this.**


***You don't need to change any options below, just submit and your merge request will be reviewed by our team***